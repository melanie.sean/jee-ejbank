package com.ejbank.api;


import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.manager.AccountManagerInt;
import com.ejbank.payload.account.AccountDetailPayload;
import com.ejbank.payload.account.AccountsPayload;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountInfo {
	
	@EJB
	private AccountManagerInt am;
	
    @GET
    @Path("accounts/{userId}")
    public AccountsPayload getAccounts(@PathParam("userId") Integer userId) {
        return am.getAccounts(userId);
    }

    @GET
    @Path("accounts/attached/{userId}")
    public AccountsPayload getAttachedAccounts(@PathParam("userId") Integer userId) {
        return am.getAttachedAccounts(userId);
    }

    @GET
    @Path("accounts/all/{userId}")
    public AccountsPayload getAllAccounts(@PathParam("userId") Integer userId) {
        return am.getAllAccounts(userId);
    }

    @GET
    @Path("account/{account_id}/{user_id}")
    public AccountDetailPayload getAccountDetails(@PathParam("account_id") Integer accountId, @PathParam("user_id") Integer userId) {
        return am.getAccountDetail(accountId, userId);
    }

}