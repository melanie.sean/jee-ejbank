package com.ejbank.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.manager.UserManagerInt;
import com.ejbank.payload.UserPayload;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class UserInfo {

	@EJB
	private UserManagerInt um;
	
    @GET
    @Path("/{userId}")
    public UserPayload getUser(@PathParam("userId") Integer userId) {
    	// get user
    	UserPayload user = um.getUser(userId);
        return user;
    }
}
