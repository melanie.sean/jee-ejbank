package com.ejbank.api;

import com.ejbank.manager.TransactionManager;
import com.ejbank.model.RequestApplyTransaction;
import com.ejbank.model.RequestPreviewTransaction;
import com.ejbank.model.RequestValidationTransaction;
import com.ejbank.payload.transaction.ResponseApplyTransactionPayload;
import com.ejbank.payload.transaction.ResponsePreviewTransactionPayload;
import com.ejbank.payload.transaction.TransactionsPayload;
import com.ejbank.payload.transaction.ResponseValidationTransactionPayload;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class TransactionInfo {

    @EJB
    private TransactionManager tm;

    @GET
    @Path("/list/{account_id}/{offset}/{user_id}")
    public TransactionsPayload getListTransactions(@PathParam("account_id") Integer accountId, @PathParam("offset") Integer offset, @PathParam("user_id") Integer userId) {
        return tm.getTransactionsForUser(accountId, offset, userId);
    }

    @POST
    @Path("/preview")
    public ResponsePreviewTransactionPayload preview(RequestPreviewTransaction request) {
        return tm.preview(request);
    }

    @POST
    @Path("/apply")
    public ResponseApplyTransactionPayload apply(RequestApplyTransaction request) {
        return tm.commit(request);
    }

    @POST
    @Path("/validation")
    public ResponseValidationTransactionPayload validation(RequestValidationTransaction request) {
        return tm.validate(request);
    }

    @GET
    @Path("/validation/notification/{user_id}")
    public Integer getNotifications(@PathParam("user_id") Integer userId) {
        return tm.notification(userId);
    }

}
