package com.ejbank.model;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "ejbank_customer")
@DiscriminatorValue("customer")
public class Customer extends User implements Serializable {

	@ManyToOne
	@JoinColumn(name = "advisor_id")
	private Advisor advisor;

	@OneToMany(mappedBy = "customer")
	private List<Account> accounts;

	@JsonIgnore
	public Advisor getAdvisor(){
		return advisor;
	}
	@JsonIgnore
	public List<Account> getAccounts() {
		return accounts;
	}

}
