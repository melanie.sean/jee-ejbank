package com.ejbank.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@Table(name = "ejbank_account")
public class Account implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "account_type_id")
	private AccountType accountType;

	@Column(name = "balance")
	private BigDecimal balance;

	public int getId() {
		return id;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getOwnerLabel(){ return customer.getFirstname() + ' ' + customer.getLastname(); }
}
