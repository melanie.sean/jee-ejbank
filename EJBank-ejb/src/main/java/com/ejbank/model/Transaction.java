package com.ejbank.model;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "account_id_from")
	private Account accountFrom;

	@ManyToOne
	@JoinColumn(name = "account_id_to")
	private Account accountTo;

	@ManyToOne
	@JoinColumn(name = "author")
	private User author;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "comment")
	private String comment;

	@Column(name = "applied")
	private boolean applied;

	@Column(name = "date")
	private Date date;

	public Transaction(){};

	public Transaction(Account accountFrom, Account accountTo, User author, BigDecimal amount, String comment, boolean applied, Date date) {
		this.accountFrom = accountFrom;
		this.accountTo = accountTo;
		this.author = author;
		this.amount = amount;
		this.comment = comment;
		this.applied = applied;
		this.date = date;
	}

	public int getId() {
		return id;
	}
	@JsonIgnore
	public Account getAccountFrom() {
		return accountFrom;
	}
	@JsonIgnore
	public Account getAccountTo() {
		return accountTo;
	}

	public User getAuthor() {
		return author;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getComment() {
		return comment;
	}

	public boolean getApplied() {
		return applied;
	}

	public Date getDate() {
		return date;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setApplied(boolean applied) {
		this.applied = applied;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
