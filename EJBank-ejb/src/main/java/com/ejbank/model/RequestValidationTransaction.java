package com.ejbank.model;

public class RequestValidationTransaction {

    private int transaction;
    private boolean approve;
    private int author;

    public RequestValidationTransaction() {}

    public RequestValidationTransaction(int transaction, boolean approve, int author) {
        this.transaction = transaction;
        this.approve = approve;
        this.author = author;
    }

    public int getTransaction() {
        return transaction;
    }

    public boolean getApprove() {
        return approve;
    }

    public int getAuthor() {
        return author;
    }

    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public void setAuthor(int author) {
        this.author = author;
    }
}
