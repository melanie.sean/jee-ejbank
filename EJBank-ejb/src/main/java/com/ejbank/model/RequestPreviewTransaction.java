package com.ejbank.model;

public class RequestPreviewTransaction {
    private int source;
    private int destination;
    private float amount;
    private int author;

    public RequestPreviewTransaction(){}

    public RequestPreviewTransaction(int source, int destination, float amount, int author) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
        this.author = author;
    }

    public int getSource() {
        return source;
    }

    public int getDestination() {
        return destination;
    }

    public float getAmount() {
        return amount;
    }

    public int getAuthor() {
        return author;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

}