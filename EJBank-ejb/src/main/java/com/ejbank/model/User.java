package com.ejbank.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "ejbank_user")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type")
@DiscriminatorValue("none")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column( name = "login")
	private String login;
	@Column( name = "password")
	private String password;
	@Column( name = "email")
	private String email;
	@Column( name = "firstname")
	private String firstname;
	@Column( name = "lastname")
	private String lastname;

	@Column( name = "type")
	private String type;

	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}

}
