package com.ejbank.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ejbank_advisor")
@DiscriminatorValue("advisor")
public class Advisor extends User {

    @OneToMany(mappedBy = "advisor")
    private List<Customer> customers;

    public List<Customer> getCustomers() {
        return customers;
    }

}
