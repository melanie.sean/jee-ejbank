package com.ejbank.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_account_type")
public class AccountType implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name ="name")
	private String name;

	@Column(name ="rate")
	private BigDecimal rate;

	@Column(name ="overdraft")
	private Integer overdraft;

	//@OneToMany
	//@JoinColumn(name = "id", referencedColumn = "")
	//private Collection<Account> accounts;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getRate(){
		return rate;
	}

	public Integer getOverdraft() {
		return overdraft;
	}
}
