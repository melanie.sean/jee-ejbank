package com.ejbank.manager;

import com.ejbank.model.RequestApplyTransaction;
import com.ejbank.model.RequestPreviewTransaction;
import com.ejbank.payload.transaction.ResponseApplyTransactionPayload;
import com.ejbank.payload.transaction.ResponsePreviewTransactionPayload;
import com.ejbank.payload.transaction.TransactionsPayload;

public interface TransactionManagerInt {

    TransactionsPayload getTransactionsForUser(Integer accountId, Integer offset, Integer userId);

    ResponsePreviewTransactionPayload preview(RequestPreviewTransaction request);

    ResponseApplyTransactionPayload commit(RequestApplyTransaction request);

}
