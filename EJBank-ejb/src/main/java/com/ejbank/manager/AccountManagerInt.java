package com.ejbank.manager;

import com.ejbank.payload.account.AccountDetailPayload;
import com.ejbank.payload.account.AccountsPayload;

import javax.ejb.Local;

@Local
public interface AccountManagerInt {

	AccountsPayload getAccounts(Integer userId);

	AccountsPayload getAttachedAccounts(Integer userId);

	AccountsPayload getAllAccounts(Integer userId);

	AccountDetailPayload getAccountDetail(Integer accountId, Integer userId);;
}
