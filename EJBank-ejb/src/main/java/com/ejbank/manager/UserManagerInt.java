package com.ejbank.manager;

import javax.ejb.Local;

import com.ejbank.payload.UserPayload;

@Local
public interface UserManagerInt {

	UserPayload getUser(Integer userId);
	
}
