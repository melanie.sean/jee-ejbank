package com.ejbank.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ejbank.model.*;
import com.ejbank.payload.account.*;

@LocalBean
@Stateless
public class AccountManager implements AccountManagerInt {

	@PersistenceContext(unitName = "EJBankPU")
	private EntityManager em;

	private Optional<Customer> retrieveCustomer(Integer id){
		Customer customer = em.find(Customer.class, id);
		if( customer == null ){
			return Optional.empty();
		}
		return Optional.of(customer);
	}

	private Optional<Advisor> retrieveAdvisor(Integer id){
		Advisor advisor = em.find(Advisor.class, id);
		if( advisor == null ){
			return Optional.empty();
		}
		return Optional.of(advisor);
	}

	private AccountsPayload getAccountsForUser(Integer userId) {
		List<Account> accounts = em.find(Customer.class, userId).getAccounts();

		List<AccountPayload> payloads = new ArrayList<>();

		for( Account a : accounts ){
			payloads.add(new UserAccountPayload(a.getId(), a.getAccountType(), a.getBalance()));
		}

		return new AccountsPayload(payloads);
	}

	@Override
	public AccountsPayload getAccounts(Integer userId){
		Optional<Advisor> optionalAdvisor = retrieveAdvisor(userId);
		if( !optionalAdvisor.isEmpty() ){
			return new AccountsPayload("You are not a customer");
		}
		return getAccountsForUser(userId);
	}

	@Override
	public AccountsPayload getAttachedAccounts(Integer userId){
		Optional<Advisor> optionalAdvisor = retrieveAdvisor(userId);
		if( optionalAdvisor.isEmpty() ){
			return new AccountsPayload("You are not an advisor");
		}
		List<Customer> customers = optionalAdvisor.get().getCustomers();

		List<AccountPayload> payloads = new ArrayList<>();
		for( Customer c : customers ){
			for( Account a : c.getAccounts() ) {

				Integer count = em.createQuery("SELECT t FROM Transaction t WHERE t.author=:user", Transaction.class)
						.setParameter("user", c)
						.getResultList()
						.size();

				payloads.add(new AttachedAccountPayload(a.getId(), a.getAccountType(), a.getBalance(), c, count));
			}
		}
		
		return new AccountsPayload(payloads);
	}

	@Override
	public AccountsPayload getAllAccounts(Integer userId) {
		Optional<Advisor> optionalAdvisor = retrieveAdvisor(userId);
		// If he is not an advisor
		if( optionalAdvisor.isEmpty() ){
			Optional<Customer> optionalCustomer = retrieveCustomer(userId);
			// If he's not a customer too, send error
			if(optionalCustomer.isEmpty()){
				return new AccountsPayload("Unknown user");
			}
			// Else, he's a customer so it's okay
			return getAccountsForUser(userId);
		}
		// Else, an advisor was found

		List<Customer> customers = optionalAdvisor.get().getCustomers();
		List<AccountPayload> payloads = new ArrayList<>();

		for( Customer c : customers ) {
			for( Account a : c.getAccounts() ) {
				payloads.add(new AdvisorAccountPayload(a.getId(), a.getAccountType(), a.getBalance(), c));
			}
		}

		return new AccountsPayload(payloads);
	}

	@Override
	public AccountDetailPayload getAccountDetail(Integer accountId, Integer userId) {
		Account account = em.find(Account.class, accountId);

		if(account == null) {
			return new AccountDetailPayload("");
		}

		BigDecimal interest = BigDecimal.ZERO; //TODO
		String owner = account.getCustomer().getFirstname() + " " + account.getCustomer().getLastname();
		String advisor = account.getCustomer().getAdvisor().getFirstname() + " " + account.getCustomer().getAdvisor().getLastname();

		return new AccountDetailPayload(owner, advisor, account.getAccountType().getRate(), interest, account.getBalance(), null);
	}

}
