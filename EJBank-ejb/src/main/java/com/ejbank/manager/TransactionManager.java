package com.ejbank.manager;

import com.ejbank.model.*;
import com.ejbank.payload.transaction.ResponseApplyTransactionPayload;
import com.ejbank.payload.transaction.ResponsePreviewTransactionPayload;
import com.ejbank.payload.transaction.TransactionPayload;
import com.ejbank.payload.transaction.TransactionsPayload;
import com.ejbank.payload.transaction.ResponseValidationTransactionPayload;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@LocalBean
@Stateless
public class TransactionManager implements TransactionManagerInt {

    private static final String SUCCESS_TRANSACTION     = "Vous disposez d'un solde suffisant.";
    private static final String FAILURE_TRANSACTION     = "Vous ne disposez pas d'un solde suffisant.";

    private static final String VALIDATED_TRANSACTION   = "Transaction validée";
    private static final String UNVALIDATED_TRANSACTION = "Transaction non validée";
    private static final String WAITING_APPROVAL        = "Votre transaction doit être validée par un conseiller.";
    private static final String NOT_ADVISOR             = "Vous n'êtes pas conseiller";

    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    private Optional<Customer> retrieveCustomer(Integer id){
        Customer customer = em.find(Customer.class, id);
        if( customer == null ){
            return Optional.empty();
        }
        return Optional.of(customer);
    }

    private Optional<Advisor> retrieveAdvisor(Integer id){
        Advisor advisor = em.find(Advisor.class, id);
        if( advisor == null ){
            return Optional.empty();
        }
        return Optional.of(advisor);
    }

    private String getTruc(Account account, Integer accountId){
        return account.getId() == accountId ?
                null :
                account.getOwnerLabel();
    }

    @Override
    public TransactionsPayload getTransactionsForUser(Integer accountId, Integer offset, Integer userId){
        boolean isCustomer = true;
        if( offset < 0) {
            return new TransactionsPayload("offset is negative wtf ?");
        }

        Account account = em.find(Account.class, accountId);
        if( account == null ){
            return new TransactionsPayload("Compte non trouvé");
        }

        List<Transaction> transactions = em.createQuery("SELECT t FROM Transaction t WHERE t.accountFrom=:account OR t.accountTo=:account", Transaction.class)
                .setParameter("account", accountId)
                .setFirstResult(offset)
                .getResultList();

        Optional<Customer> optionalCustomer = retrieveCustomer(userId);
        Optional<Advisor> optionalAdvisor;
        // If he's not a customer too, send error
        if(optionalCustomer.isEmpty()){
            isCustomer = false;
            optionalAdvisor = retrieveAdvisor(userId);
            if(optionalAdvisor.isEmpty()){
                return new TransactionsPayload("Unknown user");
            }
        }

        List<TransactionPayload> payloads = new ArrayList<>();

        for( Transaction t : transactions ){
            StateTransaction state = t.getApplied() ? StateTransaction.APPLYED : ( isCustomer ? StateTransaction.WAITING_APPROVE : StateTransaction.TO_APPROVE );

            String source = isCustomer ?
                    getTruc(t.getAccountFrom(), accountId) :
                    t.getAccountFrom().getOwnerLabel();

            String destination = isCustomer ?
                    getTruc(t.getAccountTo(), accountId) :
                    t.getAccountTo().getOwnerLabel();

            payloads.add(
                    new TransactionPayload(
                            t.getId(),
                            t.getDate(),
                            t.getAccountFrom(),
                            t.getAccountTo(),
                            t.getAmount(),
                            t.getAuthor(),
                            t.getComment(),
                            state,
                            source,
                            destination
                    )
            );
        }

        return new TransactionsPayload(payloads.size(), payloads);
    }

    @Override
    public ResponsePreviewTransactionPayload preview(RequestPreviewTransaction request) {
        boolean result;
        String message;

        float before, after;

        Account source = em.find(Account.class, request.getSource());
        Account destination = em.find(Account.class, request.getDestination());

        before = source.getBalance().floatValue() - request.getAmount();
        after = destination.getBalance().floatValue() + request.getAmount();

        if (before < 0) {
            result = false;
            message = FAILURE_TRANSACTION;
        } else {
            result = true;
            message = SUCCESS_TRANSACTION;
        }

        return new ResponsePreviewTransactionPayload(result, before, after, message);
    }

    @Override
    public ResponseApplyTransactionPayload commit(RequestApplyTransaction request) {
        boolean applied, isCustomer = true;

        Optional<Customer> optionalCustomer = retrieveCustomer(request.getAuthor());
        Optional<Advisor> optionalAdvisor;
        // If he's not a customer too, send error
        if(optionalCustomer.isEmpty()){
            isCustomer = false;
            optionalAdvisor = retrieveAdvisor(request.getAuthor());
            if(optionalAdvisor.isEmpty()){
                return new ResponseApplyTransactionPayload(false, "Unknown user");
            }
        }

        String message;
        BigDecimal afterSource, afterDestination;

        Account source = em.find(Account.class, request.getSource());
        Account destination = em.find(Account.class, request.getDestination());
        User author = em.find(User.class, request.getAuthor());

        afterSource = source.getBalance().subtract(request.getAmount());
        afterDestination = destination.getBalance().add(request.getAmount());

        if (afterSource.intValue() < 0) {
            return new ResponseApplyTransactionPayload(false, FAILURE_TRANSACTION);
        }

        message = SUCCESS_TRANSACTION;

        // Si l'utilisateur est un customer et transaction > à 1000€ => applied = false => waiting to approve
        applied = !(isCustomer && request.getAmount().intValue() > 1000);

        Date date = new Date(System.currentTimeMillis());
        Transaction transaction = new Transaction(
                source,
                destination,
                author,
                request.getAmount(),
                request.getComment(),
                applied,
                date
        );
        em.persist(transaction);

        // Si la transaction est immédiatement validée, mettre à jour les comptes
        if( applied ){
            // update source
            source.setBalance(afterSource);
            // update destination
            destination.setBalance(afterDestination);
            em.flush();
        }

        return new ResponseApplyTransactionPayload(true, message);
    }

    public ResponseValidationTransactionPayload validate(RequestValidationTransaction request) {
        // Vérifier si t'es pas un advisor
        Optional<Advisor> optionalAdvisor = retrieveAdvisor(request.getAuthor());

        // Si pas advisor => Exploser !
        if(optionalAdvisor.isEmpty()){
            return new ResponseValidationTransactionPayload(false, UNVALIDATED_TRANSACTION, NOT_ADVISOR);
        }

        // Mise à jour de l'objet transaction
        Transaction transaction = em.find(Transaction.class, request.getTransaction());
        transaction.setApplied(request.getApprove());
        em.flush();
        em.merge(transaction);

        String message = request.getApprove() ? VALIDATED_TRANSACTION : WAITING_APPROVAL;

        return new ResponseValidationTransactionPayload(true, message);
    }

    public Integer notification(Integer userId) {

        User user = em.find(User.class, userId);

        Integer count = em.createQuery("SELECT t FROM Transaction t WHERE t.author=:user", Transaction.class)
                .setParameter("user", user)
                .getResultList()
                .size();

        return count;
    }

}
