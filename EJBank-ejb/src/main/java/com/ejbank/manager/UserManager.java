package com.ejbank.manager;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ejbank.model.User;
import com.ejbank.payload.UserPayload;


@LocalBean
@Stateless
public class UserManager implements UserManagerInt {

	@PersistenceContext(unitName = "EJBankPU")
	private EntityManager em;
	
	public UserPayload getUser(Integer userId){
		User user = em.find(User.class, userId);
		return new UserPayload(user.getLastname(), user.getFirstname()) ;
	}
}