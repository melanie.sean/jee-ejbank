package com.ejbank.payload.account;

import com.ejbank.model.AccountType;
import com.ejbank.model.User;

import java.math.BigDecimal;

public class AttachedAccountPayload extends AdvisorAccountPayload implements AccountPayload {

    private final int validation;

    public AttachedAccountPayload(Integer id, AccountType accountType, BigDecimal balance, User user, int validation) {
        super(id, accountType, balance, user);
        this.validation = validation;
    }

    public Integer getValidation() {
        return validation;
    }

}