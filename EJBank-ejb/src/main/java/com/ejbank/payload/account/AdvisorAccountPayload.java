package com.ejbank.payload.account;

import com.ejbank.model.AccountType;
import com.ejbank.model.User;

import java.math.BigDecimal;

public class AdvisorAccountPayload extends UserAccountPayload implements AccountPayload {

    private final User user;

    public AdvisorAccountPayload(Integer id, AccountType accountType, BigDecimal balance, User user) {
        super(id, accountType, balance);
        this.user = user;
    }

    public String getUser(){
        return user.getFirstname() + ' ' + user.getLastname();
    }

}
