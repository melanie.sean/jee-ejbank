package com.ejbank.payload.account;

import com.ejbank.model.AccountType;

import java.math.BigDecimal;

public class UserAccountPayload implements AccountPayload{

	private final Integer id;
	private final AccountType type;
	private final BigDecimal amount;

	public UserAccountPayload(Integer id, AccountType accountType, BigDecimal balance) {
        this.id = id;
        this.type = accountType;
        this.amount = balance;
    }

	public Integer getId() {
		return id;
	}

	public String getType() {
		return type.getName();
	}

	public BigDecimal getAmount() {
		return amount;
	}
    
}
