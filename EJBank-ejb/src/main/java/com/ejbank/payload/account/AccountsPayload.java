package com.ejbank.payload.account;


import java.util.ArrayList;
import java.util.List;

public class AccountsPayload {

    private final List<AccountPayload> accounts;
    private final String error;

    public AccountsPayload(List<AccountPayload> accounts, String error) {
        this.accounts = accounts;
        this.error = error;
    }

    public AccountsPayload(List<AccountPayload> accounts) {
        this.accounts = accounts;
        this.error = "";
    }

    public AccountsPayload(String error) {
        this.accounts = new ArrayList<>();
        this.error = error;
    }

    public List<AccountPayload> getAccounts() {
        return List.copyOf(accounts);
    }

    public String getError() {
        return error;
    }

}
