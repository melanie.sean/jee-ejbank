package com.ejbank.payload.transaction;

import com.ejbank.model.Account;
import com.ejbank.model.StateTransaction;
import com.ejbank.model.User;

import java.math.BigDecimal;
import java.sql.Date;

public class TransactionPayload {

    private final int id;
    private final Date date;
    private final Account source;
    private final Account destination;
    private final BigDecimal amount;
    private final User author;
    private final String comment;
    private final StateTransaction stateTransaction;

    private final String sourceUser;
    private final String destinationUser;

    public TransactionPayload(int id, Date date, Account source, Account destination, BigDecimal amount, User author, String comment, StateTransaction stateTransaction,
            String sourceUser, String destinationUser) {
        this.id = id;
        this.date = date;
        this.source = source;
        this.destination = destination;

        this.amount = amount;
        this.author = author;

        this.comment = comment == null ? "" : comment;

        this.stateTransaction = stateTransaction;

        this.sourceUser = sourceUser;
        this.destinationUser = destinationUser;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getSource(){ return source.getAccountType().getName(); }

    public String getDestination(){ return destination.getAccountType().getName(); }

    public String getSource_user() { return source.getCustomer().getFirstname() + ' ' + source.getCustomer().getLastname(); }

    public String getDestination_user() {
        return destinationUser;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAuthor() {
        return author.getFirstname() + ' ' + author.getLastname();
    }

    public String getComment() {
        return comment;
    }

    public StateTransaction getState() {
        return stateTransaction;
    }

}
