package com.ejbank.payload.transaction;

public class ResponsePreviewTransactionPayload {
    private boolean result;
    private float before;
    private float after;
    private String message;
    private String error;

    public ResponsePreviewTransactionPayload(boolean result, float before, float after, String message) {
        this(result, before, after, message, "");
    }

    public ResponsePreviewTransactionPayload(boolean result, float before, float after, String message, String error) {
        this.result = result;
        this.before = before;
        this.after = after;
        this.message = message;
        this.error = error;
    }

    public boolean getResult() {
        return result;
    }

    public float getBefore() {
        return before;
    }

    public float getAfter() {
        return after;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }
}
