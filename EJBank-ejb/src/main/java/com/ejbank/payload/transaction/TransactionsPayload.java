package com.ejbank.payload.transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionsPayload {

    private final Integer total;
    private final List<TransactionPayload> transactions;
    private final String error;

    public TransactionsPayload(Integer total, List<TransactionPayload> transactions) {
        this.total = total;
        this.transactions = transactions;
        this.error = null;
    }

    public TransactionsPayload(String error) {
        this.total = 0;
        this.transactions = new ArrayList<>();
        this.error = error;
    }

    public Integer getTotal() {
        return total;
    }

    public List<TransactionPayload> getTransactions() {
        return List.copyOf(transactions);
    }

    public String getError() {
        return error;
    }

}