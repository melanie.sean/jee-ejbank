package com.ejbank.payload.transaction;

public class ResponseApplyTransactionPayload {

    private boolean result;
    private String message;

    public ResponseApplyTransactionPayload(boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    public boolean getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}