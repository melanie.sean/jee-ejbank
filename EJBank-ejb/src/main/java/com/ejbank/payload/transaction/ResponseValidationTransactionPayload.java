package com.ejbank.payload.transaction;

public class ResponseValidationTransactionPayload {
    private boolean result;
    private String message;
    private String error;

    public ResponseValidationTransactionPayload(boolean result, String message, String error) {
        this.result = result;
        this.message = message;
        this.error = error;
    }

    public ResponseValidationTransactionPayload(boolean result, String message) {
        this(result, message, "");
    }

    public boolean getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

}